import java.util.Arrays;

public class ArrSort {
    public static void main(String[] args) {
        int[] arr = {10, 45, 78, 2, 34, 5, 0};

        //.sort
        //.copyOf
        //.copyOfRange
        //.equals
        //.deepEquals
        //.toString

        Arrays.sort(arr);
        for (int i = 0; i < arr.length; i++) {
            System.out.println(arr[i]);
        }

        int min = 0;
        for (int i = 0; i < arr.length; i++) {
            min = (min > arr[i]) ? arr[i] : min;
        }
        System.out.println(min);

        int max = 0;
        for (int i = 0; i < arr.length; i++) {
            max = (max < arr[i]) ? arr[i] : max;
        }
        System.out.println(max);

        for (int i = arr.length-1; i > 0; i--) {
            for (int j = 0; j < i; j++) {
                if (arr[j] > arr[j+1]) {
                    int tmp = arr[j];
                    arr[j] = arr[j+1];
                    arr[j+1] = tmp;
                }
            }
        }
        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i] + " ");
        }

        System.out.println();

        for (int i = arr.length - 1; i > 0; i--) {
            for (int j = 0; j < i; j++) {
                if (arr[j] < arr[j+1]) {
                    int tmp = arr[j+1];
                    arr[j+1] = arr[j];
                    arr[j] = tmp;
                }
            }
        }
        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i] + " ");
        }

    }
}
