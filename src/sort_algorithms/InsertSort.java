package sort_algorithms;

public class InsertSort {
    public static void main(String[] args) {

        int[] arr = {12, 13, 4, 5, 7, 90, 0, 8, -1};

        for (int i = 1; i < arr.length; i++) {
            int newElement = arr[i];
            int location = i - 1;
            while (location >= 0 && arr[location] > newElement) {
                arr[location + 1] = arr[location];
                location--;
            }
            arr[location + 1] = newElement;
        }

        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i] + " ");
        }

    }
}
