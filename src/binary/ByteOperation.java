package binary;

import java.util.Scanner;

public class ByteOperation {
    public static void main(String[] args) {

        System.out.println(Integer.toBinaryString(31));

        int index = 0;

       /* Scanner scanner = new Scanner(System.in);
        int num = scanner.nextInt();

        for (int i = 7; i >= 0; i--) {
            System.out.print((num >> i) & 1);

            //System.out.print((num & (1 << i)) >> i);

            //1 => (1 << i) => 1 << 5 (32)
            //2 => num & 32
            //2 => (либо 32 либо 0) >> i

        }*/

        System.out.println(Integer.parseInt("11111", 2));

        String myString2 = "11111";
        int num = 0;
        for (int i = 0; i < myString2.length(); i++) {
            num = (num << 1) + (myString2.charAt(i) == '0' ? 0 : 1);
            //1
            //2 + 1 = 3
            //6 + 1 = 7
            //14 + 1 = 15
            //30 + 1 = 31
        }
        System.out.println(num);

    }

    public static void perform(int i) {

    }

    public static void complex(int[] buffer) {
        
    }
}
